// $Id $

Description
-------
The Booklists module is used to create listing pages and blocks of popular books, DVDs, music, etc. with deep links to Amazon and a library catalog.

This module is maintained by the Virtual Services team at Richland Library (http://www.richlandlibrary.com) in South Carolina.
The coding for the Drupal 7 module was originally written by Mark W. Jarrell (http://drupal.org/user/249768).

Install
-------
Installing the Booklists module:

1) Download the Booklists module.

2) Download the dependency modules (Amazon, jQuery Update, Libraries, and QueryPath) which are required for Booklists module to function. These can be found at the following URLS:
http://drupal.org/project/amazon
http://drupal.org/project/jquery_update
http://drupal.org/project/querypath

2) Copy the booklists and dependency module folders to the sites/all/contrib/modules folder in your installation.

3) Enable the modules using the Modules administration page (/admin/modules).

4) Configure settings for the module on the Booklists settings page (/admin/config/user-interface/booklists).

Support
-------
If you experience a problem with the module, file a
request or issue on the Booklists issue queue (http://drupal.org/project/issues/booklists).
DO NOT POST IN THE FORUMS. Posting in the issue queue is a direct line of
communication with the module author.